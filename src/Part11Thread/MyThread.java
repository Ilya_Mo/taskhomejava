package Part11Thread;

/* Cоздать синхронизированый метод который будет использоватся в 2 разных потоках */

public class MyThread implements Runnable {
    private int result;

    @Override
    public void run() {

        for (int i = 0; i < 10; i++);
        {
            a();
                    System.out.println("Синхрон-батон " + result);
        }
    }

    synchronized void a()
    {
        int i = result;
        result = i + i;
    }

    public static void main(String[] args) {
        MyThread mt = new MyThread();

        Thread t = new Thread(mt);

        t.start();
    }
}
