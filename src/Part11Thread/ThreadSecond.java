package Part11Thread;

/* Создать метод которы будет в паралеле создавать массивы из 5 рандомных значений
количество потоков 10
Генерация данных  происходит паралельно, используется один и тот же метод
метод возвращает разные значения каждому из потоков */

public class ThreadSecond implements Runnable {

    @Override
    public void run() {

        ArowGen();
    }

    private synchronized int[] ArowGen() {
        int[] mas = new int[5];
        for (int i = 0; i < mas.length; i++) {
            mas[i] = (int) (Math.random() * 10);
            System.out.println(mas[i]);
        }
        return mas;
    }

    public static void main(String[] args) {

        ThreadSecond testTwo = new ThreadSecond();
        Thread one = new Thread(testTwo);
        Thread two = new Thread(testTwo);
        Thread three = new Thread(testTwo);
        Thread four = new Thread(testTwo);
        Thread five = new Thread(testTwo);

        one.start();

        two.start();

        three.start();

        four.start();

        five.start();
    }
}
