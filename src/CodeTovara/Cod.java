package CodeTovara;

import java.util.Scanner;

/*  Вывести цену за определенный товар опираясь на его код
(предусмотреть возможность введение неверного кода). */

public class Cod {
    public static void main(String[] args) {
        int m;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число ");
        m = sc.nextInt();

        String a1, a2, a3;
        int b1, b2, b3;

        a1 = "120";
        a2 = "130";
        a3 = "140";
        b1 = 1; b2 = 2; b3 = 3;

        if(m == b1) {
            System.out.println(a1);
        } else {
            if(m == b2) {
                System.out.println(a2);
            } else {
                if(m == b3) {
                    System.out.println(a3);
                } else {
                    System.out.println("Извините, но данного товара нет в системе");
                }
            }
        }
    }
}
