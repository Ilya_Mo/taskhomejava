package Part5Interface;

/* Создать интерфейс звери в нем создать методы
рост
вес
движение
есть */

public interface Beasts {
    void weight();
    void height();
    void motion();
    void eat();
}
