package Part5Interface;

/* Создать класс собака и  имплементировть интерфейс звери
* Создать класс дог и переопределить  в нем основые методы*/

public class Dog implements Beasts {
    @Override
    public void weight() {
        System.out.println("Много весит");
    }

    @Override
    public void height() {
        System.out.println("Очень высокий");
    }

    @Override
    public void motion() {
        System.out.println("Двигается");
    }

    @Override
    public void eat() {
        System.out.println("Ест");
    }
}
