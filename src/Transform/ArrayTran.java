package Transform;

/* Перобразовать массив в строку и обртано в массив */

public class ArrayTran {
    public static void main(String[] args) {

        char[] a = {'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd'};
        String b = String.valueOf(a);
        System.out.println("Char Array back to String is: " + b);
        System.out.println(b.toCharArray()); // toCharArray - преобразует строку вновый массив символов

    }
}
