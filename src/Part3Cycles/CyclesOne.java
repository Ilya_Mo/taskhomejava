package Part3Cycles;

/*  Создать цикл с послеусловием и с перед условием который будет выводить число а, пока оно не станет менше 10 */

public class CyclesOne {
    public static void main(String[] args) {

        for(int a = 0; a < 10; a++) {
            System.out.println(a);
        }

    }
}
