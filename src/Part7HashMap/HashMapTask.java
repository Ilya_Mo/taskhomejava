package Part7HashMap;

import java.util.HashMap;
import java.util.Map;

public class HashMapTask {
    public static void main(String[] args) {

        Map<Integer, Integer> map = new HashMap<>();

        map.put(5, 80);
        map.put(7, 9893);

        System.out.println(map);
    }
}
