package Part4Object;

/*  Создать класс ТЕСТ  в нем реализовать метод в котором берется имя класса и добавляется параметр */

public class ObjectOne {
    public static void main(String[] args) {

        TECT a1 = new TECT();
        a1.displayInfo();

        TECT a2 = new TECT();
        a2.displayInfo();

    }
}

class TECT{

    String name;
    int age;

    {
        name = "Test";
        age = 16;
    }
    void displayInfo() {
        System.out.println(name);
    }
}
