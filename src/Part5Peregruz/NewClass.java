package Part5Peregruz;

/* перегрузка методов */

public class NewClass {
    public static void main(String[] args) {

        NewClassMethods nc = new NewClassMethods();
        nc.weight();
        nc.weight(35);
        nc.motion(4);

    }
}
