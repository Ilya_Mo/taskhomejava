package Part9Generics;
/* Создать переменную с определенным типом  */

import java.util.ArrayList;
import java.util.List;

public class MainG {
    public static void main(String[] args) {
        List<Integer> ages = new ArrayList<>();
        ages.add(20);
        System.out.println(ages);
    }
}
