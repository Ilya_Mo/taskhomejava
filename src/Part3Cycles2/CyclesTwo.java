package Part3Cycles2;

/* Создать цикл for each  для массива из 10 рандомных чисел,если число равно 5 вывести в консоль */

public class CyclesTwo {
    public static void main(String[] args) {

        int[] mass = {1, 2, 4, 11, 5, 23, 8, 66, 0, 6};

        for (int i : mass) {
            if (i == 5) {
                System.out.println(i);
            }
        }
    }
}
