package Factorial;
import java.util.Scanner;

/* Факториал числа. */

public class Factorial {
    public static void main(String[] args) {
        int n;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число ");
        n = sc.nextInt();
        int result = 1;
        for (int i = 2; i <= n; i++)
            result *= i;
        System.out.println(result);
    }
}
