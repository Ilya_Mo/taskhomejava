package Array;
import java.util.Arrays;

/* Посчитать среднее арифметическое массива */

public class ArrayTask {
    public static void main(String[] args) {
        int [] mas = new int[5];

        for (int i = 0; i < 5; i++) {
            mas[i] = (int)(Math.random()*6);
        }
        System.out.println(Arrays.toString(mas));
        double average = 0;
        int i;
        for (i = 0; i < 5; i++)
            average = average + mas[i];
        System.out.println("Среднее значение " + average/5 );
    }
}
