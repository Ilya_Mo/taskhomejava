package Part8Exceptions;

public class FinalTask {
    public static void main(String[] args) {

        try {
            taskA();
        } catch (Exception e) {

        }

    }
    static void taskA() {
        try {
            System.out.println("Inside taskA");
            throw new RuntimeException("demo");
        } finally {
            System.out.println("taskA's finally");
        }
    }
}
