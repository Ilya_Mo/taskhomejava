package IntegerCheck;

/* Вывести на экран информацию о том является ли целое
число записанное в переменную n, чётным либо нечётным */

import java.util.Scanner;

public class IntCheck {
    public static void main(String[] args) {

        int m;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число ");
        m = sc.nextInt();

        if(m%2 == 0) {
            System.out.println("Число " + m + " является четным");
        } else {
            System.out.println("Число " + m + " нечетное");
        }
    }
}
